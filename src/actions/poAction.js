import axios from 'axios';

export const PO = {
  ALL: 'PO_ALL',
  ADD: 'PO_ADD',
  DELETE: 'PO_DELETE'
};

const apiUrl = 'http://localhost:2018/po';

export const createPoSuccess = data => {
  return {
    type: PO.ADD,
    payload: {
      id_po: data._id,
      nama: data.nama,
      alamat: data.alamat,
      email: data.email,
      no_hp: data.no_hp
    }
  };
};

export const createPo = ({ nama, alamat, email, no_hp }) => {
  return dispatch => {
    return axios
      .post(`${apiUrl}/add`, { nama, alamat, email, no_hp })
      .then(response => {
        dispatch(createPoSuccess(response.data));
      })
      .catch(error => {
        throw error;
      });
  };
};

export const deletePoSuccess = id => {
  return {
    type: PO.DELETE,
    payload: {
      id
    }
  };
};

export const deletePo = id => {
  return dispatch => {
    return axios
      .delete(`${apiUrl}/delete/${id}`)
      .then(response => {
        dispatch(deletePoSuccess(response.data));
      })
      .catch(error => {
        throw error;
      });
  };
};

export const fetchPo = po => {
  return {
    type: PO.ALL,
    po
  };
};

export const fetchAllPo = () => {
  return dispatch => {
    return axios
      .get(apiUrl)
      .then(response => {
        dispatch(fetchPo(response.data));
      })
      .catch(error => {
        throw error;
      });
  };
};
