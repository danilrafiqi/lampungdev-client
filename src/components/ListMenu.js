import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Class from '@material-ui/icons/Class';
import VideoLibrary from '@material-ui/icons/VideoLibrary';
import { NavLink } from 'react-router-dom';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  cleanLink: {
    textDecoration: 'none'
  }
});

const menu = [
  {
    link: '/admin/video',
    icon: <VideoLibrary />,
    text: 'Video'
  },
  {
    link: '/admin/class',
    icon: <Class />,
    text: 'Class'
  }
];

function ListMenu(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Divider />
      <List component="nav">
        {menu.map((datas, index) => {
          return (
            <NavLink to={datas.link} className={classes.cleanLink} key={index}>
              <ListItem button>
                <ListItemIcon>{datas.icon}</ListItemIcon>
                <ListItemText primary={datas.text} />
              </ListItem>
            </NavLink>
          );
        })}
      </List>
    </div>
  );
}

ListMenu.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListMenu);
