import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router-dom';
// import BusListNew from '../containers/bus/BusListNew';
// import PoListNew from '../containers/po/PoListNew';
import ClassList from '../containers/class/ClassList';
import VideoList from '../containers/video/VideoList';
import Dashboard from '../containers/dashboard/Laporan';

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto'
  }
});

class MainRouter extends Component {
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Typography variant="display1" noWrap>
          <Switch>
            <Route exact path="/admin" component={Dashboard} />
          </Switch>

          <Switch>
            <Route path="/admin/class/" component={ClassList} />
            <Route path="/admin/video" component={VideoList} />
          </Switch>
        </Typography>
      </main>
    );
  }
}

export default withStyles(styles)(MainRouter);
