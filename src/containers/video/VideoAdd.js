import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    name: null,
    slug: null,
    urlVideo: null,
    classId: '',
    data_class: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSelectedFile = event => {
    this.setState({
      urlVideo: event.target.files[0]
    });
  };

  create = () => {
    const data = {
      name: this.state.name,
      slug: this.state.slug,
      urlVideo: this.state.urlVideo,
      classId: this.state.classId
    };

    axios.post('https://lampungdev.herokuapp.com/video/', data).then(res => {
      this.setState({
        open: false,
        name: null,
        slug: null,
        urlVideo: null,
        classId: ''
      });
      this.props.getData();
    });
  };

  getClass = () => {
    axios.get(`https://lampungdev.herokuapp.com/class/`).then(res => {
      this.setState({
        data_class: res.data
      });
    });
  };
  dataForm = [
    {
      title: 'Name',
      name: 'name',
      nilai: this.state.name
    },
    {
      title: 'Slug',
      name: 'slug',
      nilai: this.state.slug
    },
    {
      title: 'Video URL',
      name: 'urlVideo',
      nilai: this.state.urlVideo
    }
  ];
  componentWillMount() {
    this.getClass();
  }
  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">{'Add Data'}</DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              {this.dataForm.map((datas, index) => {
                return (
                  <TextField
                    key={index}
                    id="filled-name"
                    label={datas.title}
                    className={classes.textField}
                    name={datas.name}
                    value={datas.value}
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="filled"
                  />
                );
              })}

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="classId">Kelas</InputLabel>
                <Select
                  value={this.state.classId}
                  onChange={this.handleChange}
                  input={<FilledInput name="classId" id="classId" />}>
                  {this.state.data_class.map(datas => {
                    return (
                      <MenuItem key={datas._id} value={datas._id}>
                        {datas.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button onClick={this.create} color="primary" autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
