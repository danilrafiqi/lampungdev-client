import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    name: null,
    slug: null,
    urlVideo: null,
    classId: '',
    data_class: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSelectedFile = event => {
    this.setState({
      foto: event.target.files[0]
    });
  };

  detail = id => {
    axios.get(`https://lampungdev.herokuapp.com/video/${id}`).then(res => {
      console.log('resnya', res);
      this.setState({
        open: true,
        _id: res.data._id,
        name: res.data.name,
        slug: res.data.slug,
        urlVideo: res.data.urlVideo,
        classId: res.data.classId._id
      });
    });
  };

  update = id => {
    const data = {
      name: this.state.name,
      slug: this.state.slug,
      urlVideo: this.state.urlVideo,
      classId: this.state.classId
    };

    axios
      .put(`https://lampungdev.herokuapp.com/video/${id}`, data)
      .then(res => {
        this.setState({
          open: false,
          _id: '',
          name: '',
          slug: '',
          urlVideo: '',
          classId: ''
        });
        this.props.getData();
      });
  };
  getClass = () => {
    axios.get(`https://lampungdev.herokuapp.com/class/`).then(res => {
      this.setState({
        data_class: res.data
      });
    });
  };
  componentWillMount() {
    this.getClass();
  }
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">{'Update Data'}</DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Name"
                className={classes.textField}
                name="name"
                value={this.state.name}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Slug"
                className={classes.textField}
                name="slug"
                value={this.state.slug}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label="URL Video"
                className={classes.textField}
                name="urlVideo"
                value={this.state.urlVideo}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="classId">Kelas</InputLabel>
                <Select
                  value={this.state.classId}
                  onChange={this.handleChange}
                  input={<FilledInput name="classId" id="classId" />}>
                  {this.state.data_class.map(datas => {
                    return (
                      <MenuItem key={datas._id} value={datas._id}>
                        {datas.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
