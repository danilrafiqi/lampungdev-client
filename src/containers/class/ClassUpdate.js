import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    _id: '',

    name: null,
    slug: null,
    foto: null
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSelectedFile = event => {
    this.setState({
      foto: event.target.files[0]
    });
  };

  detail = id => {
    axios.get(`https://lampungdev.herokuapp.com/class/${id}`).then(res => {
      console.log('resnya', res);
      this.setState({
        open: true,
        _id: res.data._id,
        name: res.data.name,
        slug: res.data.slug,
        foto: res.data.foto
      });
    });
  };

  update = id => {
    const data = {
      name: this.state.name,
      slug: this.state.slug,
      foto: this.state.foto
    };

    axios
      .put(`https://lampungdev.herokuapp.com/class/${id}`, data)
      .then(res => {
        this.setState({
          open: false,
          _id: '',
          name: '',
          slug: '',
          foto: ''
        });
        this.props.getData();
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">{'Update Data'}</DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Name"
                className={classes.textField}
                name="name"
                value={this.state.name}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Slug"
                className={classes.textField}
                name="slug"
                value={this.state.slug}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label="Foto"
                className={classes.textField}
                name="foto"
                value={this.state.foto}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
