import { Switch, Route } from 'react-router-dom';
import React, { Component } from 'react';
import Home from './Home';
import Kelas from '../../components/CardClass';
// import ListVideo from '../../components/ListVideo';
class HomeRouter extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/kelas" component={Kelas} />
        <Route exact path="/" component={Home} />
      </Switch>
    );
  }
}

export default HomeRouter;
