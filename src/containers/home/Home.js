import React, { Component } from 'react';
import CardCLass from '../../components/CardClass';
import Jumbotron from '../../components/Jumbotron';

class Album extends Component {
  render() {
    return (
      <React.Fragment>
        <Jumbotron />
        <CardCLass />
      </React.Fragment>
    );
  }
}

export default Album;
