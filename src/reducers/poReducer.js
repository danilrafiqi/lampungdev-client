import { PO } from '../actions/poAction';

const poReducer = (state = [], action) => {
  switch (action.type) {
    case PO.ADD:
      return [...state, action.payload];
    case PO.DELETE:
      return state.filter(post => post.id_po !== action.payload.id_po);
    case PO.ALL:
      return action.po;
    default:
      return state;
  }
};

export default poReducer;
