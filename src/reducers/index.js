import { combineReducers } from 'redux';
import poReducer from './poReducer';
import busReducer from './busReducer';

const rootReducer = combineReducers({
  // user:userReducer,
  bus: busReducer,
  po: poReducer
});

export default rootReducer;
