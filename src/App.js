import React, { Component } from 'react';

import { Switch, Route } from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Home from './containers/home/HomeLayout';
import ListVideo from './components/ListVideo';
import Login from './containers/login/Login';
import PrivateRoute from './components/PrivateRoute';

import firebase from 'firebase/app';

class App extends Component {
  state = { loading: true, authenticated: false, user: null };

  componentWillMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({
          authenticated: true,
          currentUser: user,
          loading: false
        });
      } else {
        this.setState({
          authenticated: false,
          currentUser: null,
          loading: false
        });
      }
    });
  }

  render() {
    const { authenticated } = this.state;
    return (
      <Switch>
        <Route path="/login" component={Login} />
        <PrivateRoute
          path="/kelas/:kelas"
          authenticated={authenticated}
          component={ListVideo}
        />
        <Route path="/admin" component={Dashboard} />
        <Route path="/" component={Home} />
      </Switch>
    );
  }
}

export default App;
